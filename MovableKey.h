#ifndef MOVABLEKEY_H
#define MOVABLEKEY_H

//A key that can be moved around the screen
//can lock/unlock
//can be removed/added to the screen
//will light up when the corresponding key is pressed on the keyboard
//might poop on your mother

#include <QWidget>
#include <QLabel>
#include <QString>

class MovableKey : public QWidget
{
    Q_OBJECT
public:
    MovableKey(QWidget* parent = 0);
    MovableKey(QString key, QWidget* parent = 0);

    void init();
    void updateLabel();
    void lightKey();
    void unlightKey();

    QString key_;
    bool locked_;
    bool isLit_;
    QLabel* displayLabel_;
    
signals:
    
public slots:
    
};

#endif // MOVABLEKEY_H
