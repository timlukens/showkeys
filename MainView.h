#ifndef MAINVIEW_H
#define MAINVIEW_H

//just making a subclass now to keep this clean later
//when we add more functionality to the view
//like adding and removing your own keys, loading presets, etc

#include <QObject>
#include <QWidget>
#include <QEvent>
#include <vector>
#include "qmlapplicationviewer.h"
#include "KeyboardListener.h"
#include "MovableKey.h"

using namespace std;

class MainView : public QmlApplicationViewer
{
    Q_OBJECT
public:
    MainView();

    MovableKey* getKeyForEvent(QKeyEvent* event);

    KeyboardListener* keyListener_;
    vector<MovableKey*> keysVector_;

public slots:
    void gotKeyPress(QKeyEvent* event);
    void gotKeyRelease(QKeyEvent* event);
};

#endif // MAINVIEW_H
