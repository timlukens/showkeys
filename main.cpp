#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include "MainView.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    MainView viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/ShowKeys/main.qml"));
    viewer.showExpanded();



    return app->exec();
}
