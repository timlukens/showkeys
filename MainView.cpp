#include "MainView.h"
#include <QKeyEvent>

//THIS IS THE AMAZING CLASS THAT DOES EVERYTHING EVER!

MainView::MainView() : QmlApplicationViewer() {
    MovableKey* qKey = new MovableKey("q", this);
    qKey->setGeometry(50, 50, qKey->geometry().width(), qKey->geometry().height());
    qKey->show();

    MovableKey* wKey = new MovableKey("w", this);
    wKey->setGeometry(100, 50, wKey->geometry().width(), wKey->geometry().height());
    wKey->show();

    keysVector_.push_back(qKey);
    keysVector_.push_back(wKey);

    keyListener_ = new KeyboardListener(this);
    connect(keyListener_, SIGNAL(keyPressed(QKeyEvent*)), this, SLOT(gotKeyPress(QKeyEvent*)));
    connect(keyListener_, SIGNAL(keyReleased(QKeyEvent*)), this, SLOT(gotKeyRelease(QKeyEvent*)));
}

//light key
void MainView::gotKeyPress(QKeyEvent* event) {
    MovableKey* key = this->getKeyForEvent(event);
    if(key) key->lightKey();
}

//unlight key
void MainView::gotKeyRelease(QKeyEvent* event) {
    MovableKey* key = this->getKeyForEvent(event);
    if(key) key->unlightKey();
}


//return a MovableKey matching the keyboard press/release
MovableKey* MainView::getKeyForEvent(QKeyEvent* event) {
    for(int i = 0; i < keysVector_.size(); i++) {
        MovableKey* key = keysVector_[i];
        if(event->text().compare(key->key_) == 0) {
            return key;
        }
    }

    return NULL;
}
