#ifndef KEYBOARDLISTENER_H
#define KEYBOARDLISTENER_H

#include <QWidget>

class KeyboardListener : public QWidget
{
    Q_OBJECT
public:
    explicit KeyboardListener(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent* event);
    void keyReleaseEvent(QKeyEvent* event);
    
signals:
    void keyPressed(QKeyEvent* event);
    void keyReleased(QKeyEvent* event);
    
public slots:
    
};

#endif // KEYBOARDLISTENER_H
