#include "KeyboardListener.h"

KeyboardListener::KeyboardListener(QWidget *parent) : QWidget(parent) {
    this->setFocusPolicy(Qt:: StrongFocus);
}

void KeyboardListener::keyPressEvent(QKeyEvent* event) {
    emit this->keyPressed(event);
}

void KeyboardListener::keyReleaseEvent(QKeyEvent* event) {
    emit this->keyReleased(event);
}
