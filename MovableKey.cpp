#include "MovableKey.h"
#include <QTimer>

#define kKeyWidth 60
#define kKeyHeight 60
#define kKeyLabelWidth 40
#define kKeyLabelHeight 40

MovableKey::MovableKey(QWidget* parent) : QWidget(parent) {
    this->key_ = "a";
    this->init();
}

MovableKey::MovableKey(QString key, QWidget* parent) : QWidget(parent) {
    this->key_ = key;
    this->init();
}

//generic init
//I also want to draw a single pixel rectangle around the border of a MovableKey
//gotta figure out how to do that
//I assume there is some graphics drawing lib
void MovableKey::init() {
    displayLabel_ = NULL;
    locked_ = true;
    isLit_ = false;

    this->setGeometry(0,0,kKeyWidth, kKeyHeight);
    this->updateLabel();
}

void MovableKey::updateLabel() {
    //if first pass
    if(!displayLabel_) {
        displayLabel_ = new QLabel(key_, this);

        //super math to put the label smack dab in the middle of the container
        displayLabel_->setGeometry((kKeyWidth - kKeyLabelWidth) / 2,
                                   (kKeyHeight - kKeyLabelHeight) / 2,
                                   kKeyLabelWidth, kKeyLabelHeight);

        //super font to make big
        QFont font;
        font.setPointSize(32);
        displayLabel_->setFont(font);
        displayLabel_->setAlignment(Qt::AlignCenter);

        displayLabel_->show();
    }

    displayLabel_->setText(key_);

    //debug test
    //LIGHT DAT BITCH UP
    //this->lightKey();
}

void MovableKey::lightKey() {
    //wat is this stylesheet shit...
    displayLabel_->setStyleSheet("QLabel { color : red; }");
    isLit_ = true;
}

void MovableKey::unlightKey() {
    displayLabel_->setStyleSheet("QLabel { color : black; }");
    isLit_ = false;
}
